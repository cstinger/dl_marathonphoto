#!/usr/bin/python3
# For macOS temporary, will update later

from bs4 import BeautifulSoup
#import datetime
import os
import sys
from pathlib import Path
HOME = str(Path.home())

#_WDir=os.getcwd()+"/.."
_PDir=HOME+"/Pictures"
# RamDisk, please create it by yourself, or move it to some place else
_IndexDir="/Volumes/RamDisk"
_IndexFile=_IndexDir+"/bmp.html"
_IndexLnk=_IndexDir+"/bmp.link"


with open(_IndexFile, 'r') as _Index:
    contents = _Index.read()
    soup = BeautifulSoup(contents, 'html.parser')

    _fl = open(_IndexLnk,"w+")
    for link in soup.find_all('a'):
        #print(link.get('href'))
        _fl.write(link.get('href')+"\n")

    _fl.close()
_Index.close()
