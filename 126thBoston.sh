#!/bin/bash
# For macOS temporary

_PDir="$HOME/Pictures"
_BMPDir="${_PDir}/126thBostonMarathon"
_MPLink="<YourDownloadLinkHere>"
_IndexFile="/Volumes/RamDisk/bmp.html"
_IndexLnk="/Volumes/RamDisk/bmp.link"
_IndexLnk2="/Volumes/RamDisk/bmp2.link"

wget -O $_IndexFile $_MPLink

python3 ${_PDir}/getlink.py
echo "MP4 video part is not supported, jpg photos supported so far"
grep "XLarge" $_IndexLnk > $_IndexLnk2

while IFS= read -r line; do
	_FileName=`echo "$line" | awk -F '/' '{print $4"_"$8"_XLarge.jpg"}'`
	wget -c -O ${_BMPDir}/${_FileName} ${_MPLink}/../$line &
done < "$_IndexLnk2"

wait

rm $_IndexFile $_IndexLnk $_IndexLnk2
